package com.app.musicbookck.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.musicbookck.adapter.MusicAdapter
import com.app.musicbookck.model.Music
import com.app.musicbookck.utility.Utility
import com.app.musicbookck.utility.checkPermissionWriteExternal
import com.app.musicbookck.utility.customMaterialDialog
import com.app.musicbookck.utility.getAllAudio
import com.app.musicbookck.R
import com.app.musicbookck.activity.sideMenu.AboutActivity
import com.app.musicbookck.activity.sideMenu.FeedBackActivity
import com.app.musicbookck.activity.sideMenu.SettingActivity
import com.app.musicbookck.databinding.ActivityMainBinding
import com.app.musicbookck.model.MusicPlaylist
import com.app.musicbookck.utility.Generics
import com.app.musicbookck.utility.SharedPreference
import com.app.musicbookck.utility.SharedPreference.Companion.FAVOURITE
import com.app.musicbookck.utility.SharedPreference.Companion.PLAYLIST
import com.app.musicbookck.utility.sendIntentPlayActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var adapter: MusicAdapter

    companion object {
        lateinit var MusicListMA: ArrayList<Music>

        //For result
        lateinit var musicListSearch: ArrayList<Music>
        var search: Boolean = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MusicBook)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        Utility.getInstance().hideKeyBoardWhenTouchOutside(binding.clRoot)
        setContentView(binding.root)

        // adding onBackPressed callback listener.
        onBackPressedDispatcher.addCallback(this, onBackPressedCallback)

        drawerInitialize()

        this.checkPermissionWriteExternal {
            initializeLayout()
            retrievingSPData()
        }

        navigationClick()
        binding.clickListener = this
        binding.lifecycleOwner = this
    }

    private fun retrievingSPData() {

        /**
         * for retrieving favourites data using shared Preferences
         */
        FavouriteActivity.favouriteSong = ArrayList()
        val jsonString = SharedPreference.getValue(FAVOURITE, "")

        if (jsonString != "") {
            val data = Generics().getGenericList<Music>(jsonString.toString())
            FavouriteActivity.favouriteSong.addAll(data)
        }

        /**
         * for retrieving PlayList data using shared Preferences
         */
        PlayListActivity.musicPlaylist = MusicPlaylist()
        val jsonPlayList = SharedPreference.getValue(PLAYLIST, "")

        if (jsonPlayList != "") {
            val dataPlaylist = Generics().getFromJson<MusicPlaylist>(jsonPlayList.toString())
            PlayListActivity.musicPlaylist = dataPlaylist!!
            Log.d("MyValue", "retrievingSPData: $dataPlaylist")
        }
    }

    private fun drawerInitialize() {
        // for nav drawer
        toggle = ActionBarDrawerToggle(
            this, binding.drawerLayoutRoot,
            R.string.open, R.string.close
        )
        binding.drawerLayoutRoot.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val colorDrawable = ColorDrawable(Color.parseColor("#FFFFFF"))
        supportActionBar?.setBackgroundDrawable(colorDrawable)
    }

    private fun navigationClick() {
        binding.navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navFeedback -> startActivity(Intent(this, FeedBackActivity::class.java))

                R.id.navSettings -> startActivity(Intent(this, SettingActivity::class.java))

                R.id.navAbout -> startActivity(Intent(this, AboutActivity::class.java))

                R.id.navExit -> {
                    this.customMaterialDialog("Exit", "Do you want to close app?", {
                        Utility.getInstance().exitApplication()
                    })
                }
            }
            true
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initializeLayout() {
        search = false
        adapter = MusicAdapter()
        MusicListMA = this.getAllAudio()

        binding.rvSongList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            setItemViewCacheSize(10)
        }

        binding.rvSongList.adapter = adapter
        adapter.submitList(MusicListMA)
        binding.tvTotalSongNo.text = "Total Songs: ".plus(adapter.itemCount.toString())

        adapter.rootClick = {
            when {
                search -> {
                    Log.d("MyValue", "AdapterSearchClick: $it")
                    this@MainActivity.sendIntentPlayActivity("AdapterSearchClick", it)
                }

                MusicListMA[it].id == PlayerActivity.nowPlayingId -> {
                    this@MainActivity.sendIntentPlayActivity("NowPlaying", it)
//                    this@MainActivity.sendIntentPlayActivity("NowPlaying", PlayerActivity.songPosition)
                }

                else -> {
                    Log.d("MyValue", "AdapterOnItemClick: $it")
                    this@MainActivity.sendIntentPlayActivity("AdapterOnItemClick", it)
                }
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnShuffle -> {
                this.sendIntentPlayActivity("MainActivityOnShuffleClick", 0)
            }

            R.id.btnFavorite -> {
                startActivity(Intent(this, FavouriteActivity::class.java))
            }

            R.id.btnPlaylist -> {
                startActivity(Intent(this, PlayListActivity::class.java))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item))
            return true
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
         /**
          * for storing favourites data using shared Preferences
          */
//        val jsonString = GsonBuilder().create().toJson(FavouriteActivity.favouriteSong)
        val json = Generics().setToJson(FavouriteActivity.favouriteSong)
        SharedPreference.setValue(FAVOURITE, json)
        Log.d("MyValue", "onResume: $json")

        /**
         * for storing PlayList data using shared Preferences
         */
        val jsonPlayList = Generics().setToJson(PlayListActivity.musicPlaylist)
        SharedPreference.setValue(PLAYLIST, jsonPlayList)
        Log.d("MyValue", "onResumePlayList: $jsonPlayList")
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!PlayerActivity.isPlaying && PlayerActivity.musicService != null) {
            Utility.getInstance().exitApplication()
        }
    }

    //OnBackPress Function
    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            if (binding.drawerLayoutRoot.isDrawerOpen(GravityCompat.START))
                binding.drawerLayoutRoot.closeDrawer(GravityCompat.START)
            else finish()
        }
    }

    //Implement Option Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_view_menu, menu)
        val searchView = menu?.findItem(R.id.searchView)?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true

            override fun onQueryTextChange(newText: String?): Boolean {
                musicListSearch = ArrayList()
                if (newText != null) {
                    for (song in MusicListMA) {
                        if (song.title.lowercase().contains(newText.trim().lowercase()))
                            musicListSearch.add(song)
                    }
                    search = true
                    adapter.submitList(musicListSearch)
                }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

}
