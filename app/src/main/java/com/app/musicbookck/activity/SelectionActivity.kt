package com.app.musicbookck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.musicbookck.R
import com.app.musicbookck.adapter.MusicAdapter
import com.app.musicbookck.databinding.ActivitySelectionBinding
import com.app.musicbookck.utility.sendIntentPlayActivity

class SelectionActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var binding : ActivitySelectionBinding
    private lateinit var adapter: MusicAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.clickListener = this

        initializeRecyclerView()
        searchInitialize()
    }

    private fun initializeRecyclerView() {

        val playList = MainActivity.MusicListMA

        adapter = MusicAdapter(selectionActivity = true).apply {
            submitList(playList)
            playListClick = {
                this@SelectionActivity.sendIntentPlayActivity("PlaylistDetailsAdaptor", it)
            }
        }

        binding.rvSelection.apply {
            setItemViewCacheSize(10)
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SelectionActivity)
        }
        binding.rvSelection.adapter = adapter
    }

    /**
     * For search View */
    private fun searchInitialize() {
        binding.svSelection.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true

            override fun onQueryTextChange(newText: String?): Boolean {
                MainActivity.musicListSearch = ArrayList()
                if (newText != null) {
                    val userInput = newText.lowercase()
                    for (song in MainActivity.MusicListMA) {
                        if (song.title.lowercase().contains(userInput))
                            MainActivity.musicListSearch.add(song)
                    }
//                    MainActivity.search = true
                    adapter.submitList(MainActivity.musicListSearch)
                }
                return true
            }
        })
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ibBack -> finish()
        }
    }
}