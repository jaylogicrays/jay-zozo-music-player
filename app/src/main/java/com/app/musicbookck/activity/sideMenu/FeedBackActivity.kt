package com.app.musicbookck.activity.sideMenu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.musicbookck.R
import com.app.musicbookck.databinding.ActivityFeedBackBinding

class FeedBackActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var binding : ActivityFeedBackBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFeedBackBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.clickListener = this
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.ibBack -> {
                finish()
            }
        }
    }
}