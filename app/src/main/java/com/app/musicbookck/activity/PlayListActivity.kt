package com.app.musicbookck.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.app.musicbookck.R
import com.app.musicbookck.adapter.PlayListViewAdapter
import com.app.musicbookck.databinding.ActivityPlayListBinding
import com.app.musicbookck.databinding.AddPlayalistDialogBinding
import com.app.musicbookck.model.MusicPlaylist
import com.app.musicbookck.model.PlayListModel
import com.app.musicbookck.utility.customMaterialDialog
import com.app.musicbookck.utility.getCurrentDate
import com.app.musicbookck.utility.showToast
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class PlayListActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityPlayListBinding
    private lateinit var adapter : PlayListViewAdapter

    companion object {
        var musicPlaylist : MusicPlaylist = MusicPlaylist()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.clickListener = this
        initializeLayout()
    }

    private fun initializeLayout() {
        adapter = PlayListViewAdapter()

        binding.rvPlayList.setHasFixedSize(true)
        binding.rvPlayList.layoutManager = GridLayoutManager(this, 2)
        adapter.deleteClick = { deletePlayList(it) }
        adapter.submitList(musicPlaylist.ref)
        binding.rvPlayList.adapter = adapter

        adapter.rootClick = {
            val intent = Intent(this, PlayListDetailsActivity::class.java)
            intent.putExtra("index", it)
            startActivity(intent)
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id) {
            R.id.ibBack -> finish()
            R.id.fabCreatePlaylist -> {
                customAlertDialog()
            }
        }
    }

    private fun customAlertDialog() {
        val customDialog = LayoutInflater.from(this).inflate(R.layout.add_playalist_dialog, binding.clRoot, false)

        val binder = AddPlayalistDialogBinding.bind(customDialog)
        val builder = MaterialAlertDialogBuilder(this)
        builder.setView(customDialog)
            .setTitle("PlayList details")
            .setPositiveButton("Add") { dialog,_ ->
                val playListName = binder.etPlayListName.text
                val createdBy = binder.etYourName.text
                if (playListName != null && createdBy != null) {
                    if (playListName.isNotEmpty() && createdBy.isNotEmpty()) {
                        addPlayList(playListName.toString(), createdBy.toString())
                    } else
                        this.showToast("Please fill fields")
                }
                dialog.dismiss()
            }
        val dialogBuilder = builder.create()
        dialogBuilder.show()
        dialogBuilder.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED)
    }

    private fun addPlayList(name: String, createBy: String) {
        var playlistExists = false
        for(i in musicPlaylist.ref) {
            if (name == i.name) {
                playlistExists = true
                break
            }
        }

        if (playlistExists)
            this.showToast("PlayList Exist!!")
        else {
            val tempPlayList = PlayListModel()
            tempPlayList.name = name
            tempPlayList.playList = ArrayList()
            tempPlayList.createdBy = createBy
            tempPlayList.createOn = this.getCurrentDate("dd MMM yyyy")
            musicPlaylist.ref.add(tempPlayList)
            adapter.submitList(musicPlaylist.ref)
            binding.rvPlayList.adapter = adapter
        }
    }

    private fun deletePlayList(position : Int) {
        binding.rvPlayList.setItemViewCacheSize(10)
        adapter.submitList(musicPlaylist.ref)
        binding.rvPlayList.adapter = adapter

        this.customMaterialDialog(musicPlaylist.ref[position].name, "Do you want to delete this PlayList?", {
            showToast("Deleted ${musicPlaylist.ref[position].name} PlayList")
            musicPlaylist.ref.removeAt(position)
            binding.rvPlayList.adapter = adapter
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onResume() {
        super.onResume()
        adapter.notifyDataSetChanged()
    }

}