package com.app.musicbookck.activity

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.app.musicbookck.R
import com.app.musicbookck.databinding.ActivityPlayerBinding
import com.app.musicbookck.model.Music
import com.app.musicbookck.servicess.MusicService
import com.app.musicbookck.utility.Utility
import com.app.musicbookck.utility.customMaterialDialog
import com.app.musicbookck.utility.getEqualizer
import com.app.musicbookck.utility.setIconImageBtn
import com.app.musicbookck.utility.setSongPosition
import com.app.musicbookck.utility.showToast
import com.google.android.material.bottomsheet.BottomSheetDialog

class PlayerActivity : AppCompatActivity(), View.OnClickListener, ServiceConnection,
    MediaPlayer.OnCompletionListener {

    companion object {
        lateinit var musicListPA: ArrayList<Music>
        var songPosition: Int = 0
        var isPlaying: Boolean = false
        var musicService: MusicService? = null

        @SuppressLint("StaticFieldLeak")
        lateinit var binding: ActivityPlayerBinding
        var repeat: Boolean = false
        var min15 : Boolean = false
        var min30 : Boolean = false
        var min60 : Boolean = false
        var nowPlayingId : String = ""
        var isFavourite : Boolean = false
        var fIndex : Int = -1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.clickListener = this
        initializeLayout()
        setSeekBarAndListener()
    }

    private fun initializeLayout() {
        songPosition = intent.getIntExtra("index", 0)
        when (intent.getStringExtra("class")) {

            "NowPlaying" -> {
                setLayout()
                binding.apply {
                    startSongDuration = musicService!!.currentPosition().toLong()
                    seekBarPlayerActivity.progress = musicService!!.currentPosition()
                    seekBarPlayerActivity.max = musicService!!.durationMusic()
                    btnPlayPause.setIconResource(if (isPlaying) R.drawable.ic_pause else R.drawable.ic_play)
                }
                Log.d("MyValue", "NowPlaying: ${musicListPA.size} :: $songPosition")
            }

            "FavouriteAdapter" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(FavouriteActivity.favouriteSong)
                Log.d("MyValue", "FavouriteAdapter: ${musicListPA.size} :: $songPosition")
                setLayout()
            }

            "AdapterSearchClick" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(MainActivity.musicListSearch)
                Log.d("MyValue", "AdapterSearchClick: ${musicListPA.size} :: $songPosition")
                setLayout()
            }

            "AdapterOnItemClick" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(MainActivity.MusicListMA)
                setLayout()
                Log.d("MyValue", "AdapterOnItemClick: ${musicListPA.size} :: $songPosition")
            }

            "MainActivityOnShuffleClick" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(MainActivity.MusicListMA)
                Log.d("MyValue", "MainActivityOnShuffleClick: ${musicListPA.size} :: $songPosition")
                setLayout()
                musicListPA.shuffle()
            }

            "FavShuffleClick" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(FavouriteActivity.favouriteSong)
                Log.d("MyValue", "FavShuffleClick: ${musicListPA.size} :: $songPosition")
                setLayout()
                musicListPA.shuffle()
            }

            "PlayerDetailShuffle" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(PlayListActivity.musicPlaylist.ref[PlayListDetailsActivity.currentPlayListPos].playList)
                Log.d("MyValue", "PlayerDetailShuffle: ${musicListPA.size} :: $songPosition")
                setLayout()
                musicListPA.shuffle()
            }

            "PlaylistDetailsAdaptor" -> {
                doServiceStart()
                musicListPA = ArrayList()
                musicListPA.addAll(PlayListActivity.musicPlaylist.ref[PlayListDetailsActivity.currentPlayListPos].playList)
                Log.d("MyValue", "PlaylistDetailsAdaptor: ${musicListPA.size} :: $songPosition")
                setLayout()
            }
        }
    }

    private fun setLayout() {
        fIndex = Utility.getInstance().favoriteChecker(musicListPA[songPosition].id)

        binding.musicItem = musicListPA[songPosition]

        if (repeat) binding.ibRepeat.setImageResource(R.drawable.ic_repeat_on)

        if (min15 || min30 || min60 ) binding.ibTimer.setImageResource(R.drawable.ic_timer_on)

        binding.ibFavorite.setIconImageBtn(if (isFavourite) R.drawable.ic_favorite else R.drawable.ic_favorite_border_white)

    }

    private fun doServiceStart() {
        //For Starting Service
        val intent = Intent(this, MusicService::class.java)
        bindService(intent, this, BIND_AUTO_CREATE)
        startService(intent)
    }

    private fun createMediaPlayer() {
        try {
            if (musicService!!.mediaPlayer == null) musicService!!.mediaPlayer = MediaPlayer()

            musicService!!.apply {
                resetMusic()
                mediaPlayer!!.setDataSource(musicListPA[songPosition].path)
                prepareMusic()
                startMusic()
            }
            isPlaying = true

            binding.apply {
                btnPlayPause.setIconResource(R.drawable.ic_pause)
                musicItem = musicListPA[songPosition]
                startSongDuration = musicService!!.currentPosition().toLong()
                seekBarPlayerActivity.progress = 0
                seekBarPlayerActivity.max = musicService!!.durationMusic()
            }

            musicService!!.showNotification(R.drawable.ic_pause)
            musicService!!.mediaPlayer!!.setOnCompletionListener(this)

            nowPlayingId = musicListPA[songPosition].id
        } catch (e: Exception) {
            return
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ibBack -> finish()

            R.id.ibFavorite -> {
                if (isFavourite) {
                    FavouriteActivity.favouriteSong.removeAt(fIndex)
                    isFavourite = false
                    binding.ibFavorite.setImageResource(R.drawable.ic_favorite_border_white)
                } else {
                    FavouriteActivity.favouriteSong.add(musicListPA[songPosition])
                    isFavourite = true
                    binding.ibFavorite.setImageResource(R.drawable.ic_favorite)
                }
            }

            R.id.btnPlayPause -> {
                if (isPlaying) pauseMusic() else playMusic()
            }

            R.id.btnPreviousSong -> prevNextSong(false)

            R.id.btnNextSong -> prevNextSong(true)

            R.id.ibRepeat -> {
                if (repeat) {
                    binding.ibRepeat.setImageResource(R.drawable.ic_repeat_off)
                    this.showToast("Repeat is off")
                } else {
                    binding.ibRepeat.setImageResource(R.drawable.ic_repeat_on)
                    this.showToast("Repeat is on")
                }
                repeat = !repeat //if(repeat) false else true
            }

            R.id.ibEqualizer -> {
                this.getEqualizer()
            }

            R.id.ibTimer -> {
                val timer = min15 || min30 || min60

                if (!timer) showBottomSheet()
                else {
                    this.customMaterialDialog("Stop Timer","Do you want to stop timer?", {
                        min15 = false
                        min30 = false
                        min60 = false
                        binding.ibTimer.setImageResource(R.drawable.ic_timer)
                    })
                }
            }

            R.id.ibShare -> {
                val sharIntent = Intent().apply {
                    action = Intent.ACTION_SEND
                    type = "audio/*"
                    putExtra(Intent.EXTRA_STREAM, Uri.parse(musicListPA[songPosition].path))
                }
                startActivity(Intent.createChooser(sharIntent, "Sharing Music File!!"))
            }
        }
    }

    //Set Seek Bar
    private fun setSeekBarAndListener() {
        binding.seekBarPlayerActivity.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) musicService!!.mediaPlayer!!.seekTo(progress)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) = Unit
            override fun onStopTrackingTouch(p0: SeekBar?) = Unit
        })
    }

    /**
     * For Playing Music*/
    private fun playMusic() {
        binding.btnPlayPause.setIconResource(R.drawable.ic_pause)
        musicService!!.showNotification(R.drawable.ic_pause)
        isPlaying = true
        musicService!!.startMusic()
    }

    /**
     * For pause music*/
    private fun pauseMusic() {
        binding.btnPlayPause.setIconResource(R.drawable.ic_play)
        musicService!!.showNotification(R.drawable.ic_play)
        isPlaying = false
        musicService!!.pauseMusic()
    }

    /**
     * For play next song and previous song*/
    private fun prevNextSong(increment: Boolean) {
        this.setSongPosition(increment)
        setLayout()
        createMediaPlayer()
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        val binder = service as MusicService.MyBinder
        musicService = binder.currentService()
        createMediaPlayer()
        musicService!!.seekBarSetup()

        musicService!!.audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        // Request audio focus for playback
        musicService!!.audioManager.requestAudioFocus(musicService,
            AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
    }

    /**
     * for After finishing the music it moves to the next song and plays the song*/
    override fun onCompletion(mp: MediaPlayer?) {
        setSongPosition(true)
        createMediaPlayer()
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        musicService = null
    }

    //For set sleep Timer (15 min, 30min and 1hour(60min))
    private fun showBottomSheet() {
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(R.layout.bottom_sheet_time)

        val timers = arrayOf(15, 30, 60)
        val timerTextViews = arrayOf(R.id.tv15min, R.id.tv30min, R.id.tv60min)

        for (i in timers.indices) {
            val timerValue = timers[i]
            val textViewId = timerTextViews[i]

            dialog.findViewById<TextView>(textViewId)?.setOnClickListener {
                this.showToast("Music will stop after $timerValue min")
                binding.ibTimer.setImageResource(R.drawable.ic_timer_on)
                setTimer(timerValue)
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun setTimer(minutes: Int) {
        when (minutes) {
            15 -> min15 = true
            30 -> min30 = true
            60 -> min60 = true
        }
        Utility.getInstance().sleepTimer(minutes.toLong(), min15 || min30 || min60)
    }

    //Activity Result for Equalizer
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 27 || resultCode == RESULT_OK) {
            return
        }
    }

}
