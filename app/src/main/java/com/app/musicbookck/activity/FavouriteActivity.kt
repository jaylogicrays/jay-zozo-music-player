package com.app.musicbookck.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.musicbookck.adapter.FavouriteMusicAdapter
import com.app.musicbookck.model.Music
import com.app.musicbookck.R
import com.app.musicbookck.databinding.ActivityFavouriteBinding
import com.app.musicbookck.utility.Utility
import com.app.musicbookck.utility.hideView
import com.app.musicbookck.utility.sendIntentPlayActivity

class FavouriteActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityFavouriteBinding
    private lateinit var adapter: FavouriteMusicAdapter

    companion object {
        var favouriteSong: ArrayList<Music> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFavouriteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.clickListener = this
        /** Check this song is exists in file or not*/
        favouriteSong = Utility.getInstance().checkPlayList(favouriteSong)
        initializeLayout()
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ibBack -> finish()

            R.id.fabShuffleFav -> {
                this.sendIntentPlayActivity("FavShuffleClick", 0)
            }
        }
    }

    private fun initializeLayout() {
        if (favouriteSong.size < 1) binding.fabShuffleFav.hideView()
        adapter = FavouriteMusicAdapter().apply {
            submitList(favouriteSong)
            itemClick = {
                this@FavouriteActivity.sendIntentPlayActivity("FavouriteAdapter", it)
            }
        }

        binding.rvFavourite.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@FavouriteActivity)
            setItemViewCacheSize(10)
        }

        binding.rvFavourite.adapter = adapter

    }
}