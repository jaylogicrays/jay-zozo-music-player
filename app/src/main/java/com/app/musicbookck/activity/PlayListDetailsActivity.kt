package com.app.musicbookck.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.musicbookck.R
import com.app.musicbookck.adapter.MusicAdapter
import com.app.musicbookck.databinding.ActivityPlayListDetailsBinding
import com.app.musicbookck.utility.Generics
import com.app.musicbookck.utility.SharedPreference
import com.app.musicbookck.utility.Utility
import com.app.musicbookck.utility.sendIntentPlayActivity
import com.app.musicbookck.utility.showView

class PlayListDetailsActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var binding: ActivityPlayListDetailsBinding
    private var dominantColor: Int = 0
    private lateinit var adapter: MusicAdapter

    companion object {
        var currentPlayListPos: Int = -1
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayListDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.clickListener = this
        currentPlayListPos = intent.extras?.get("index") as Int
        /** Check this song is exists in file or not*/
        PlayListActivity.musicPlaylist.ref[currentPlayListPos].playList = Utility.getInstance().checkPlayList(PlayListActivity.musicPlaylist.ref[currentPlayListPos].playList)
        initializeRecyclerView()
    }

    private fun initializeRecyclerView() {

        val playList = PlayListActivity.musicPlaylist.ref[currentPlayListPos].playList

        adapter = MusicAdapter(true).apply {
            submitList(playList)
            playListClick = {
                this@PlayListDetailsActivity.sendIntentPlayActivity("PlaylistDetailsAdaptor", it)
            }
        }

        binding.rvPlayListDetails.apply {
            setItemViewCacheSize(10)
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@PlayListDetailsActivity)
        }
        binding.rvPlayListDetails.adapter = adapter
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onResume() {
        super.onResume()
        val playList = PlayListActivity.musicPlaylist.ref[currentPlayListPos]
        setUpActionBar(playList.name)
        binding.apply {
            tvTotalPLD.text = "Total ${adapter.itemCount} Songs"
            tvCreatedOn.text = "Created On: ${playList.createOn}"
            tvCreatedBy.text = "Created By: ${playList.createdBy}"

            if (adapter.itemCount > 0) {
                setDynamicColor(playList.playList[0].artUri)
                fabShufflePLD.showView()
                musicList = playList.playList[0]
            }
        }
        adapter.notifyDataSetChanged()
        /**
         * for storing PlayList data using shared Preferences
         */
        val jsonPlayList = Generics().setToJson(PlayListActivity.musicPlaylist)
        SharedPreference.setValue(SharedPreference.PLAYLIST, jsonPlayList)
        Log.d("MyValue", "onResumePlayListClass: $jsonPlayList")
    }

    private fun setDynamicColor(url: String) {
        val bitmap = try {
            val image = this.contentResolver.openInputStream(Uri.parse(url))
            BitmapFactory.decodeStream(image)
        } catch (e: Exception) {
            BitmapFactory.decodeResource(resources, R.drawable.ic_round_music_slash_screen)
        }

        Palette.from(bitmap).generate {
            it?.let { palette ->
                dominantColor =
                    palette.getDominantColor(ContextCompat.getColor(this, R.color.white))
                binding.collapsingToolbar.apply {
                    setContentScrimColor(dominantColor)
                    setStatusBarScrimColor(dominantColor)
                    val vibrant: Palette.Swatch? = palette.vibrantSwatch
                    if (vibrant != null) {
                        setCollapsedTitleTextColor(vibrant.titleTextColor)
                        setExpandedTitleColor(vibrant.titleTextColor)
                    }
                }
            }
//            val vibrantColor = palette!!.getVibrantColor(R.color.colorPrimary)
//            binding.collapsingToolbar.setContentScrimColor(vibrantColor)
//            binding.collapsingToolbar.setStatusBarScrimColor(R.color.black)
        }
    }

    private fun setUpActionBar(name: String) {
        setSupportActionBar(binding.animToolBar)
        if (supportActionBar != null)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.collapsingToolbar.title = name
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.fabShufflePLD -> {
                this.sendIntentPlayActivity("PlayerDetailShuffle", 0)
            }

            R.id.btnAddPLD -> {
                startActivity(Intent(this, SelectionActivity::class.java))
            }
        }
    }
}
