package com.app.musicbookck.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.app.musicbookck.activity.PlayerActivity
import com.app.musicbookck.fragment.NowPlayingFragment
import com.app.musicbookck.utility.Constants.EXIT
import com.app.musicbookck.utility.Constants.NEXT
import com.app.musicbookck.utility.Constants.PLAY
import com.app.musicbookck.utility.Constants.PREVIOUS
import com.app.musicbookck.utility.Utility
import com.app.musicbookck.utility.setSongPosition
import com.app.musicbookck.R

class NotificationReceiverBroad : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action) {
            PREVIOUS -> prevNextSong(false, context!!)
            NEXT -> prevNextSong(true, context!!)
            PLAY -> if (PlayerActivity.isPlaying) pauseMusic() else playMusic()
            EXIT -> Utility.getInstance().exitApplication()
        }
    }

    private fun playMusic() {
        PlayerActivity.apply {
            isPlaying = true
            musicService!!.startMusic()
            musicService!!.showNotification(R.drawable.ic_pause)
            binding.btnPlayPause.setIconResource(R.drawable.ic_pause)
        }
        NowPlayingFragment.binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_pause)
    }

    private fun pauseMusic() {
        PlayerActivity.apply {
            isPlaying = false
            musicService!!.pauseMusic()
            musicService!!.showNotification(R.drawable.ic_play)
            binding.btnPlayPause.setIconResource(R.drawable.ic_play)
        }
        NowPlayingFragment.binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_play)
    }

    private fun prevNextSong(increment: Boolean, context: Context) {
        context.setSongPosition(increment)
        PlayerActivity.apply {
            musicService!!.createMediaPlayer()
            binding.musicItem = musicListPA[songPosition]
            NowPlayingFragment.binding.musicItem = musicListPA[songPosition]
            playMusic()
            fIndex = Utility.getInstance().favoriteChecker(musicListPA[songPosition].id)
            binding.ibFavorite.setImageResource(if (isFavourite) R.drawable.ic_favorite else R.drawable.ic_favorite_border_white)
        }
    }

}