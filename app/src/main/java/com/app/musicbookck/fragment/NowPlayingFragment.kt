package com.app.musicbookck.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.musicbookck.activity.PlayerActivity
import com.app.musicbookck.utility.makeViewInvisible
import com.app.musicbookck.utility.showView
import com.app.musicbookck.R
import com.app.musicbookck.databinding.FragmentNowPlayingBinding
import com.app.musicbookck.utility.sendIntentPlayActivity

class NowPlayingFragment : Fragment() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var binding: FragmentNowPlayingBinding
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_now_playing, container, false)
        binding = FragmentNowPlayingBinding.bind(view)
        binding.root.makeViewInvisible()

        binding.btnPlayPauseNPF.setOnClickListener {
            if (PlayerActivity.isPlaying) pauseMusic() else playMusic()
        }

        binding.root.setOnClickListener {
            requireActivity().sendIntentPlayActivity("NowPlaying", PlayerActivity.songPosition)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        if (PlayerActivity.musicService != null) {
            binding.tvSongNameNPF.isSelected = true //For move text
            binding.root.showView()
            binding.musicItem = PlayerActivity.musicListPA[PlayerActivity.songPosition]
            if (PlayerActivity.isPlaying)
                binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_pause)
            else
                binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_play)
        }
    }

    private fun playMusic() {
        binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_pause)
        PlayerActivity.apply {
            musicService!!.startMusic()
            musicService!!.showNotification(R.drawable.ic_pause)
            binding.btnPlayPause.setIconResource(R.drawable.ic_pause)
            isPlaying = true
        }
    }

    private fun pauseMusic() {
        binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_play)
        PlayerActivity.apply {
            musicService!!.pauseMusic()
            musicService!!.showNotification(R.drawable.ic_play)
            binding.btnPlayPause.setIconResource(R.drawable.ic_play)
            isPlaying = false
        }
    }
}