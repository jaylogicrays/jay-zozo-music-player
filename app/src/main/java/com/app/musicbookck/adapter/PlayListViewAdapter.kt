package com.app.musicbookck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.musicbookck.activity.PlayListActivity
import com.app.musicbookck.databinding.PlaylistViewBinding
import com.app.musicbookck.model.PlayListModel

class PlayListViewAdapter : ListAdapter<PlayListModel, PlayListViewAdapter.MyViewHolder>(MyDiffUtil()){

    var rootClick: ((itemPotion: Int) -> Unit) ? = null
    var deleteClick: ((itemPotion: Int) -> Unit) ? = null

    class MyViewHolder(myBinding : PlaylistViewBinding) : RecyclerView.ViewHolder(myBinding.root) {
        val binding = myBinding
    }

    class MyDiffUtil : DiffUtil.ItemCallback<PlayListModel>() {
        override fun areItemsTheSame(oldItem: PlayListModel, newItem: PlayListModel): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: PlayListModel, newItem: PlayListModel): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
      val view  = PlaylistViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
      return  MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.musicList = currentList[position]
        holder.binding.tvPlayListName.isSelected = true
        holder.binding.root.setOnClickListener {
            rootClick!!(position)
        }
        holder.binding.ibDeletePl.setOnClickListener {
            deleteClick!!(position)
        }
        if (PlayListActivity.musicPlaylist.ref[position].playList.size > 0) {
            holder.binding.image = PlayListActivity.musicPlaylist.ref[position].playList[0].artUri
        }
    }

}