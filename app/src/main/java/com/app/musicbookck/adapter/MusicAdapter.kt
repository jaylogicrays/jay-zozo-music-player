package com.app.musicbookck.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.musicbookck.R
import com.app.musicbookck.activity.PlayListActivity
import com.app.musicbookck.activity.PlayListDetailsActivity
import com.app.musicbookck.model.Music
import com.app.musicbookck.databinding.MusicviewBinding

class MusicAdapter(private val playListDetails : Boolean = false, private val selectionActivity : Boolean = false)
    : ListAdapter<Music, MusicAdapter.MyViewHolder>(MyDiffUtill()) {

    var rootClick: ((itemPotion: Int) -> Unit) ? = null
    var playListClick : ((itemPotion: Int) -> Unit) ? = null

    class MyViewHolder(myBinding : MusicviewBinding) : RecyclerView.ViewHolder(myBinding.root) {
        val binding = myBinding
    }

    class MyDiffUtill : DiffUtil.ItemCallback<Music>() {
        override fun areItemsTheSame(oldItem: Music, newItem: Music): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Music, newItem: Music): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = MusicviewBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return MyViewHolder(view)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.musicList = currentList[position]
        when {
             playListDetails ->{
                 holder.binding.root.setOnClickListener {
                     playListClick!!(position)
                 }
             }
            selectionActivity -> {
                holder.binding.root.setOnClickListener {
                    holder.binding.root.setBackgroundColor(
                        ContextCompat.getColor(holder.itemView.context,
                            if (addSong(currentList[position])) R.color.colorGray else R.color.colorWhite))
                }
            }
            else -> {
                holder.binding.root.setOnClickListener {
                    Log.d("MyValue", "onBindViewHolder: $position")
                    rootClick!!(position)
                //  rootClick!!.invoke(position)
                }
            }
        }
    }

    private fun addSong(song: Music): Boolean {
        val playList = PlayListActivity.musicPlaylist.ref[PlayListDetailsActivity.currentPlayListPos].playList
        playList.forEachIndexed { index, music ->
            if (song.id == music.id) {
                playList.removeAt(index)
                return false
            }
        }
        playList.add(song)
        return true
    }

}