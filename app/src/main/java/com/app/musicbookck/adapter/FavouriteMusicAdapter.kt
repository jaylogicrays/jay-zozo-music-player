package com.app.musicbookck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.musicbookck.model.Music
import com.app.musicbookck.databinding.FavouriteViewBinding

class FavouriteMusicAdapter : ListAdapter<Music, FavouriteMusicAdapter.MyViewHolder>(MyDiffUtil()) {

    var itemClick: ((itemPotion: Int) -> Unit) ? = null

    class MyViewHolder(myBinding : FavouriteViewBinding) : RecyclerView.ViewHolder(myBinding.root) {
        val binding = myBinding
    }

    class MyDiffUtil : DiffUtil.ItemCallback<Music>() {
        override fun areItemsTheSame(oldItem: Music, newItem: Music): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Music, newItem: Music): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = FavouriteViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.musicList = currentList[position]
        holder.binding.root.setOnClickListener {
            itemClick!!(position)
        }
    }
}