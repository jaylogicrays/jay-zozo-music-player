package com.app.musicbookck.model

data class Music(val id: String, val title: String, val album: String,
                 val artists : String, val duration: Long = 0, val path: String,
    val artUri: String)