package com.app.musicbookck.model

class PlayListModel {
    lateinit var name : String
    lateinit var playList : ArrayList<Music>
    lateinit var createdBy : String
    lateinit var createOn : String
}

class MusicPlaylist {
    var ref : ArrayList<PlayListModel>  = ArrayList()
}