package com.app.musicbookck

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.app.musicbookck.utility.Constants.CHANNEL_ID

class ApplicationClass : Application() {

    companion object {

        lateinit var appInstance: ApplicationClass

        @Synchronized
        fun getInstance(): ApplicationClass {
            return appInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        appInstance = this
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
           val notificationChannel = NotificationChannel(CHANNEL_ID, "Now Playing Song", NotificationManager.IMPORTANCE_HIGH)
           notificationChannel.description = "This is important channel for showing song!!"
           val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
           notificationManager.createNotificationChannel(notificationChannel)
       }
    }

}