package com.app.musicbookck.utility

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.media.MediaMetadataRetriever
import android.media.audiofx.AudioEffect
import android.net.Uri
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.app.musicbookck.activity.PlayerActivity
import com.app.musicbookck.model.Music
import com.app.musicbookck.permission.PermissionCheck
import com.app.musicbookck.permission.PermissionHandler
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.concurrent.TimeUnit

fun View?.showView() {
    this?.visibility = View.VISIBLE
}

fun View?.hideView() {
    this?.visibility = View.GONE
}

fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun View?.makeViewInvisible() {
    this?.visibility = View.INVISIBLE
}

fun ImageButton.setIconImageBtn(image: Int) {
    setImageResource(image)
}

fun Context.getCurrentDate(format : String): String {
    val calender = Calendar.getInstance().time
    val sdf = SimpleDateFormat(format, Locale.ENGLISH)
    return sdf.format(calender)
}

fun View.linearGradientBackground(dominantColor : Int): GradientDrawable {

    return GradientDrawable().apply {
        colors = intArrayOf(
            dominantColor,
//            dominantColor,
//            Color.parseColor("#2E2929"),
            Color.parseColor("#171616")
        )
        gradientType = GradientDrawable.LINEAR_GRADIENT
        orientation = GradientDrawable.Orientation.TOP_BOTTOM
    }}

fun View.backgroundForFragment(dominantColor : Int): GradientDrawable {

    return GradientDrawable().apply {
        colors = intArrayOf(dominantColor, dominantColor)
//        colors = intArrayOf(
//            dominantColor, Color.parseColor("#2E2929"),
//            Color.parseColor("#171616"))
        gradientType = GradientDrawable.LINEAR_GRADIENT
        orientation = GradientDrawable.Orientation.TOP_BOTTOM
    }}

/**
 *  gives callbacks whenever any changes are recorded in the given EditText
 */
fun EditText.textChangeListener(onTextChange: (s: CharSequence?) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChange.invoke(s)
        }
    })
}

//Get All Audios in local system storage
@SuppressLint("Recycle", "Range")
fun Context.getAllAudio() : ArrayList<Music> {
    val tempList = ArrayList<Music>()

    val selection = MediaStore.Audio.Media.IS_MUSIC + "!=0"
    val projection =  arrayOf(
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.ALBUM,
        MediaStore.Audio.Media.ARTIST,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.DATE_ADDED,
        MediaStore.Audio.Media.DATA,
        MediaStore.Audio.Media.ALBUM_ID)

    val cursor = this.contentResolver.query(
        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection, null,
        MediaStore.Audio.Media.DATE_ADDED + " DESC", null)

    if (cursor !== null) {
        if (cursor.moveToFirst()) {
            do {
                val titleC =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                val idC = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media._ID))
                val albumC =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
                val artistsC =
                    cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                val pathC = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))
                val durationC =
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                val albumIDC =
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))
                        .toString()
                val uri = Uri.parse("content://media/external/audio/albumart")
                val artUriC = Uri.withAppendedPath(uri, albumIDC).toString()
                val music = Music(idC, titleC, albumC, artistsC, durationC, pathC, artUriC)
                val file = File(music.path)
                if (file.exists()) {
                    tempList.add(music)
                }
            } while (cursor.moveToNext())
            cursor.close()
        }
    }
    return tempList
}

fun Activity.hideKeyboard() {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = this.currentFocus
    if (view == null) {
        view = View(this)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * Get run time Permission storage/Audio media
 */
fun Context.checkPermissionWriteExternal(onGranted: () -> Unit = {}) {
    PermissionCheck.check(this, Utility.getInstance().getPermissionForSDK33() , "",

        object : PermissionHandler() {
            override fun onGranted() {
                onGranted.invoke()
            }

            override fun onDenied(context: Context, deniedPermissions: java.util.ArrayList<String>) {
                super.onPermissionDenied(context, deniedPermissions)
            }
        }
    )
}

fun Activity.sendIntentPlayActivity(ref: String, pos: Int) {
    Log.d("MyValue", "sendIntent: $ref :: $pos")
    this.checkPermissionWriteExternal {
        val intent = Intent(this, PlayerActivity::class.java)
        intent.putExtra("index", pos)
        intent.putExtra("class", ref)
        startActivity(intent)
    }
}

fun Context.getImageArt(path : String): ByteArray? {
    val retriever = MediaMetadataRetriever()
    retriever.setDataSource(path)
    return retriever.embeddedPicture
}

fun Context.formatDuration(duration: Long) : String {
    val minutes = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS)
    val seconds = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS) - minutes* TimeUnit.SECONDS.convert(1, TimeUnit.MINUTES)
    return String.format("%2d:%2d", minutes,seconds)
}

fun Context.setSongPosition(increment: Boolean) {
    if (!PlayerActivity.repeat) {
        if (increment) {
            if (PlayerActivity.musicListPA.size - 1 == PlayerActivity.songPosition) {
                PlayerActivity.songPosition = 0
            } else {
                ++PlayerActivity.songPosition
            }
        } else {
            if (PlayerActivity.songPosition == 0) {
                PlayerActivity.songPosition = PlayerActivity.musicListPA.size - 1
            } else {
                --PlayerActivity.songPosition
            }
        }
    }
}

//  Material Alert Dialog
fun Context.customMaterialDialog(title: String, msg: String, positiveClick: () -> Unit,
     positiveBtnText : String? = "Yes", negativeBtnText : String? = "No",
     positiveBtnColor: Int? = Color.RED, negativeBtnColor: Int? = Color.GRAY ) {
    val builder = MaterialAlertDialogBuilder(this)
    builder.setTitle(title)
        .setMessage(msg)
        .setPositiveButton(positiveBtnText) { _, _->
            positiveClick()
        }
        .setNegativeButton(negativeBtnText) {dialog, _ ->
            dialog.dismiss()
        }
    val customDialog = builder.create()
    customDialog.show()
    customDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(positiveBtnColor!!)
    customDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(negativeBtnColor!!)
}

// Change audio effects using system equalizer
fun Activity.getEqualizer() {
    try {
        val eqIntent = Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL)
        eqIntent.putExtra(
            AudioEffect.EXTRA_AUDIO_SESSION,
            PlayerActivity.musicService!!.mediaPlayer!!.audioSessionId
        )
        eqIntent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, this.packageName)
        eqIntent.putExtra(AudioEffect.EXTRA_CONTENT_TYPE, AudioEffect.CONTENT_TYPE_MUSIC)
        this.startActivityForResult(eqIntent, 27)
    } catch (e: Exception) {
        Toast.makeText(this, "Equalizer Feature not supported!!", Toast.LENGTH_SHORT).show()
    }
}
