package com.app.musicbookck.utility

object Constants {
    const val CHANNEL_ID = "Music channel"
    const val PLAY = "PLAY"
    const val NEXT = "NEXT"
    const val PREVIOUS = "PREVIOUS"
    const val EXIT = "EXIT"

    const val SWIPE_THRESHOLD = 100
    const val SWIPE_VELOCITY_THRESHOLD = 100
}