package com.app.musicbookck.utility

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.app.musicbookck.ApplicationClass
import com.app.musicbookck.activity.FavouriteActivity
import com.app.musicbookck.activity.PlayerActivity
import com.app.musicbookck.model.Music
import java.io.File
import kotlin.system.exitProcess

class Utility {

    companion object {
        private var instance: Utility? = null

        fun getInstance(): Utility {
            if (instance == null) {
                instance = Utility()
            }
            return instance as Utility
        }

        fun isPortraitMode(context: Context): Boolean =
            context.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
    }

    fun getPermissionForSDK33(): ArrayList<String> {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            arrayListOf( Manifest.permission.READ_MEDIA_AUDIO,
            Manifest.permission.POST_NOTIFICATIONS)
        } else {
            arrayListOf ( Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    fun exitApplication() {
        if (PlayerActivity.musicService != null) {

            //stop Audio checker functionality
            PlayerActivity.musicService!!.audioManager.abandonAudioFocus(PlayerActivity.musicService)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                PlayerActivity.musicService!!.stopForeground(Service.STOP_FOREGROUND_REMOVE)
            } else {
                PlayerActivity.musicService!!.stopForeground(true)
            }

            PlayerActivity.musicService!!.mediaPlayer!!.release()
            PlayerActivity.musicService = null
            exitProcess(1)
        }
    }

    fun sleepTimer(sleepTimeMills: Long, howLong: Boolean) {
        Thread{Thread.sleep(sleepTimeMills * 60000)
            if (howLong) exitApplication() }
            .start()
    }

    fun favoriteChecker(id : String) : Int {
        PlayerActivity.isFavourite = false
        FavouriteActivity.favouriteSong.forEachIndexed { index, music ->
            if (id == music.id) {
                PlayerActivity.isFavourite = true
            }
            return index
        }
        return -1
    }
    /**
     * if user particular song delete in file
     * so that time this fun check in added
     * in fav and playlist song is exists in file or not
     * "Check this song is exists in file or not" */
    fun checkPlayList(playlist : ArrayList<Music>) : ArrayList<Music> {
        playlist.forEachIndexed { index, music ->
            val file = File(music.path)
            if (!file.exists())
                playlist.removeAt(index)
        }
        return playlist
    }

    /*This method is open your keyboard on click of edittext
   * @param pass your context
   * @Param pass your edittext id
   */
    fun launchKeyboard(mContext: Activity?, mEditText: EditText) {
        mEditText.postDelayed({
            val keyboard =
                mContext!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            keyboard.showSoftInput(mEditText, 0)
        }, 100)
    }


    /*This method is hide your keyboard for particular view
     * @Param pass your view id which you want to hide
     */
    private fun hideKeyboard(v: View) {
        val mgr = ApplicationClass.getInstance()
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        mgr.hideSoftInputFromWindow(v.windowToken, 0)
    }

    @SuppressLint("ClickableViewAccessibility")
    fun hideKeyBoardWhenTouchOutside(view: View) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { v, _ ->
                hideKeyboard(v)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                hideKeyBoardWhenTouchOutside(innerView)
            }
        }
    }

    /*This method is hide your keyboard on outside touch of screen
    * @Param pass your parent view id of activity of fragment
    */
    fun outSideTouchHideKeyboard(view: View) {
        // Set up touch listener for non-text box views to hideAnimateDialog keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { p0, p1 ->
                when (p1?.action) {
                    MotionEvent.ACTION_DOWN -> {}
                    MotionEvent.ACTION_UP -> p0?.performClick()
                    else -> {}
                }
                true
            }
        }

        // If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                outSideTouchHideKeyboard(innerView)
            }
        }
    }

}
