package com.app.musicbookck.utility

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.alpha
import androidx.databinding.BindingAdapter
import androidx.palette.graphics.Palette
import com.app.musicbookck.R
import com.app.musicbookck.activity.PlayerActivity
import com.app.musicbookck.fragment.NowPlayingFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.util.concurrent.TimeUnit

@BindingAdapter("setImage")
fun AppCompatImageView.setImage(url: String?) {
    if (url != null) {
        Glide.with(this).load(url).
                apply(RequestOptions().placeholder(R.drawable.ic_round_music_slash_screen).centerCrop())
            .into(this)
    } else {
        setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_round_music_slash_screen,
                null
            )
        )
    }
}

@SuppressLint("ResourceAsColor")
@BindingAdapter("setBgColor")
fun View.setBgColor(url: String?) {
    var dominantColor = 0
    try {
        val image = context.contentResolver.openInputStream(Uri.parse(url))
        val getBitmap = BitmapFactory.decodeStream(image)

        Palette.Builder(getBitmap).generate {
            it?.let { palette ->
                dominantColor = palette.getDarkMutedColor(Color.BLACK)
                this.background = this.backgroundForFragment(dominantColor)
            }
        }
    } catch (e: Exception) {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_round_music_slash_screen)

        Palette.from(bitmap).generate {
            it?.let { palette ->
                dominantColor = palette.getDominantColor(ContextCompat.getColor(context, R.color.white) )
                this.background = this.backgroundForFragment(dominantColor)
            }
        }
    }
}


@BindingAdapter("setBgPlayerScreen")
fun View.setBgScreenColor(imageUri: String?) {

    var dominantColor = 0
    try {
        val image = context.contentResolver.openInputStream(Uri.parse(imageUri))
        val getBitmap = BitmapFactory.decodeStream(image)

        Palette.Builder(getBitmap).generate {
            it?.let { palette ->
                dominantColor = palette.getDominantColor(Color.GRAY)
                this.background = this.linearGradientBackground(dominantColor)
            }
        }
    } catch (e: Exception) {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_round_music_slash_screen)
        Palette.from(bitmap).generate {
            it?.let { palette ->
                dominantColor = palette.getDominantColor(Color.GRAY)
                this.background = this.linearGradientBackground(dominantColor)
            }
        }
    }

}

@BindingAdapter("setDurations")
fun TextView.formatDuration(duration: Long)  {
    val minutes = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS)
    val seconds = TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS) - minutes* TimeUnit.SECONDS.convert(1, TimeUnit.MINUTES)

    this.text = String.format("%02d:%02d", minutes,seconds)
}