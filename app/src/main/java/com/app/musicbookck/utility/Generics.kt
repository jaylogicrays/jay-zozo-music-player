package com.app.musicbookck.utility

import com.app.musicbookck.model.MusicPlaylist
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class Generics{

    inline fun <reified T> getDataFromSharedPreferences(key: String): T? {

        val gson = Gson()
        val json = SharedPreference.getValue(key, "")
        return if (json != null) {
            gson.fromJson(json.toString(), T::class.java)
        } else {
            null
        }
    }

    inline fun <reified T> saveDataToSharedPreferences(key: String, data: T) {
        val gson = Gson()
        val json = gson.toJson(data)
        SharedPreference.setValue(key, json)
    }

    // Use for String to Json/data class convertor
    inline fun <reified T> getFromJson(json: String): T? {
        return if (json != null) {
            Gson().fromJson(json, T::class.java)
        } else null
    }

    // Use for Json/data class to String convertor
    inline fun <reified T> setToJson(data: T): String {
        return Gson().toJson(data)
    }

    inline fun <reified T> Gson.fromJson(json: String) : T =
        this.fromJson<T>(json, T::class.java)

// ========================================================

    inline fun <reified T> getGenericList(json : String): List<T> {

        //Must use map here because the result is a list of LinkedTreeMaps
        val list: ArrayList<Map<String, Any?>>? = fromJsonWithType(json)

        //handle type erasure
        val result = list?.mapNotNull {
            mapToObject(it, T::class.java)
        }

        return  result ?: listOf()
    }

    inline fun <reified T> fromJsonWithType(json: String): T? {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun <T> mapToObject(map: Map<String, Any?>?, type: Class<T>): T? {
        if (map == null) return null

        val json = Gson().toJson(map)
        return Gson().fromJson(json, type)
    }

}
