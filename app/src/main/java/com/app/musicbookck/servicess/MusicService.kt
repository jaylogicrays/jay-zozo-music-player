package com.app.musicbookck.servicess

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import com.app.musicbookck.activity.PlayerActivity
import com.app.musicbookck.broadcast.NotificationReceiverBroad
import com.app.musicbookck.utility.Constants.CHANNEL_ID
import com.app.musicbookck.utility.Constants.EXIT
import com.app.musicbookck.utility.Constants.NEXT
import com.app.musicbookck.utility.Constants.PLAY
import com.app.musicbookck.utility.Constants.PREVIOUS
import com.app.musicbookck.utility.getImageArt
import com.app.musicbookck.R
import com.app.musicbookck.activity.MainActivity
import com.app.musicbookck.activity.PlayerActivity.Companion.musicService
import com.app.musicbookck.fragment.NowPlayingFragment
import com.app.musicbookck.utility.sendIntentPlayActivity

class MusicService : Service(), AudioManager.OnAudioFocusChangeListener {

    private var myBinder = MyBinder()
    var mediaPlayer: MediaPlayer? = null
    private lateinit var mediaSession: MediaSessionCompat
    private lateinit var runnable: Runnable
    lateinit var audioManager: AudioManager

    override fun onBind(intent: Intent): IBinder {
        mediaSession = MediaSessionCompat(baseContext, "MusicBook")
        return myBinder
    }

    inner class MyBinder : Binder() {
        fun currentService(): MusicService {
            return this@MusicService
        }
    }

    fun showNotification(playPause: Int) {
        val intent = Intent(baseContext, MainActivity::class.java)
        val contentIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)

        val prevPendingIntent = createNotificationPendingIntent(PREVIOUS)
        val playPendingIntent = createNotificationPendingIntent(PLAY)
        val nextPendingIntent = createNotificationPendingIntent(NEXT)
        val exitPendingIntent = createNotificationPendingIntent(EXIT)

        val imageArt = getImageArt(PlayerActivity.musicListPA[PlayerActivity.songPosition].path)
        val imageLargeIcon = if (imageArt != null) {
            BitmapFactory.decodeByteArray(imageArt, 0, imageArt.size)
        } else {
            BitmapFactory.decodeResource(resources, R.drawable.ic_round_music_slash_screen)
        }

        val notification = NotificationCompat.Builder(baseContext, CHANNEL_ID)
            .setContentIntent(contentIntent)
            .setContentTitle(PlayerActivity.musicListPA[PlayerActivity.songPosition].title)
            .setContentText(PlayerActivity.musicListPA[PlayerActivity.songPosition].artists)
            .setSmallIcon(R.drawable.ic_music_note)
            .setLargeIcon(imageLargeIcon)
            .setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                .setMediaSession(mediaSession.sessionToken))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setOnlyAlertOnce(true)
            .addAction(R.drawable.ic_skip_previous, "Previous", prevPendingIntent)
            .addAction(playPause, "PlayPause", playPendingIntent)
            .addAction(R.drawable.ic_skip_next_24, "Next", nextPendingIntent)
            .addAction(R.drawable.ic_exit_to_app, "Exit", exitPendingIntent)
            .build()

        startForeground(27, notification)
    }

    private fun createNotificationPendingIntent(action: String): PendingIntent {
        val intent = Intent(baseContext, NotificationReceiverBroad::class.java).setAction(action)
        return PendingIntent.getBroadcast(
            baseContext, 0, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    fun startMusic() {
        mediaPlayer!!.start()
    }

    fun pauseMusic() {
        mediaPlayer!!.pause()
    }

    fun resetMusic() {
        mediaPlayer!!.reset()
    }

    fun stopMusic() {
        mediaPlayer!!.stop()
    }

    fun prepareMusic() {
        mediaPlayer!!.prepare()
    }

    fun durationMusic(): Int {
        return mediaPlayer!!.duration
    }

    fun currentPosition(): Int {
        return mediaPlayer!!.currentPosition
    }

    fun isPlayingMusic(): Boolean {
        return mediaPlayer!!.isPlaying
    }

    fun createMediaPlayer() {
        try {
            PlayerActivity.apply {
                if (musicService!!.mediaPlayer == null) musicService!!.mediaPlayer = MediaPlayer()
                musicService!!.resetMusic()
                musicService!!.mediaPlayer!!.setDataSource(musicListPA[songPosition].path)
                musicService!!.prepareMusic()
                binding.btnPlayPause.setIconResource(R.drawable.ic_pause)
                musicService!!.showNotification(R.drawable.ic_pause)
                binding.seekBarPlayerActivity.progress = 0
                binding.seekBarPlayerActivity.max = durationMusic()
                nowPlayingId = musicListPA[songPosition].id
            }
        } catch (e: Exception) {
            return
        }
    }

    fun seekBarSetup() {
        runnable = Runnable {
            PlayerActivity.binding.apply {
                startSongDuration = currentPosition().toLong()
                seekBarPlayerActivity.progress = currentPosition()
            }
            Handler(Looper.getMainLooper()).postDelayed(runnable, 200)
        }
        Handler(Looper.getMainLooper()).postDelayed(runnable, 0)
    }


    /**
     * for using PLAY/PAUSE music when receiving any kind of calls etc.
     */
    override fun onAudioFocusChange(focusChange: Int) {
        if (focusChange <= 0) {
            //Pause music
            PlayerActivity.binding.btnPlayPause.setIconResource(R.drawable.ic_play)
            NowPlayingFragment.binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_play)
            showNotification(R.drawable.ic_play)
            PlayerActivity.isPlaying = false
            pauseMusic()
        } else {
            //Play Music
            PlayerActivity.binding.btnPlayPause.setIconResource(R.drawable.ic_pause)
            NowPlayingFragment.binding.btnPlayPauseNPF.setImageResource(R.drawable.ic_pause)
            showNotification(R.drawable.ic_pause)
            PlayerActivity.isPlaying = true
            startMusic()
        }
    }

}